#!/usr/bin/env python

from distutils.core import setup

setup(
    name="simple_html",
    version="1.0.0",
    license="MIT",
    description="Simple HTML generator",
    author="Keith Philpott",
    url="https://gitlab.com/keithasaurus/simple_html",
    packages=["simple_html"],
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    install_requires=[],
)
