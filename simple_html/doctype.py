def doctype(details: str) -> str:
    return f"<!doctype {details}>"
